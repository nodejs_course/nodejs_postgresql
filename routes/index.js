var express = require('express');
var router = express.Router();

const { Pool, Client } = require('pg');
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'nodepostgre',
  password: 'vuiLTla9',
  port: 5432,
});


/* GET home page. */
router.get('/', function(req, res, next) {
  pool.query('SELECT * from contact', (err, res) => {
    console.log(err, res)
    pool.end()
  });
  //res.render('index', { title: 'Express' });
});

/* GET addnew page. */
router.get('/addnew', function(req, res, next) {
  res.render('addnew', { title: 'Create New' });
});

/* POST addnew page. */
router.post('/addnew', function(req, res, next) {
  var name = req.body.name;
  var age = req.body.age;
  
  pool.query('INSERT INTO contact(name, age) VALUES ($1,$2)',[name, age], (err, res) => {
    console.log(err, res)
    //pool.end() 
  });
  res.redirect('home');
});

/* GET home page. */
router.get('/home', function(req, res, next) {
  pool.query('SELECT * FROM contact ORDER BY id ASC', (err, result) => {      
    console.log(err);    
    res.render('home', {title: 'List of contacts', data: result.rows })
  });
});

/* GET remove page. */
router.get('/remove/:id', function(req, res, next) { 
  var id = req.params.id;
  pool.query('DELETE FROM contact WHERE id = $1', [id], (err, result) => {
    console.log(err);    
    //pool.end();
    res.redirect('/home');
  }); 
});

/* GET edit page. */
router.get('/edit/:id', function(req, res, next) {
  var id = req.params.id;
  pool.query('SELECT * FROM contact WHERE id = $1', [id], (err, result) => {    
    res.render('modify', { title: 'Modiying', data: result.rows });
  });
});

/* POST edit page. */
router.post('/edit/:id', function(req, res, next) {
  var id = req.params.id;
  var name = req.body.name;
  var age = req.body.age;

  pool.query('UPDATE contact SET name = $1, age = $2 WHERE id = $3', [name, age, id], (err, result) => {
    console.log(err);        
    res.redirect('/home'); 
  });
});


module.exports = router;

